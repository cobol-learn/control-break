       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CB1.
       AUTHOR. PAKAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT2-FILE   ASSIGN TO "input2.txt"
           ORGANIZATION   IS LINE  SEQUENTIAL.

       
       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT2-FILE.
       01  INPUT2-BUFFER.
       88 END-OF-FILE VALUE HIGH-VALUE .
           05 COL-A PIC   X(2).
           05 COL-B PIC   X(2).
           05 COL-COUNT   PIC   9(3).

       WORKING-STORAGE SECTION. 
       01  TOTAL PIC   9(4)  VALUE ZEROS .
       01  COL-A-TOTAL PIC   9(4).
       01  COL-A-PROCESSING PIC  X(2).
       01  COL-B-TOTAL PIC   9(4).
       01  COL-B-PROCESSING PIC  X(2).

       01  RPT-ROW.
           05 PRT-COL-A   PIC BBBX(2).
           05 FILLER   PIC   X(3) VALUE SPACES.
           05 PRT-COL-B   PIC BBBX(2).
           05 FILLER   PIC   X(3) VALUE SPACES.
           05 RPT-COL-TOTAL PIC ZZZ9.

       01  RPT-A-TOTAL-ROW.
           05 FILLER   PIC X(2) VALUE SPACES .
           05 FILLER   PIC X(14) VALUE "        TOTAL:".
           05 RPT-A-TOTAL PIC ZZZ9.

       01  RPT-HEADER.
           05 FILLER   PIC X(4) VALUE "   A".
           05 FILLER   PIC   X(5) VALUE SPACES.
           05 FILLER   PIC X(3) VALUE "  B".
           05 FILLER   PIC   X(5) VALUE SPACES.
           05 FILLER PIC X(5) VALUE  "TOTAL".
       01  RPT-FOOTER.
           05 FILLER   PIC X(2) VALUE SPACES .
           05 FILLER   PIC X(14) VALUE "        TOTAL:".
           05 RPT-TOTAL PIC ZZZ9.

       
       PROCEDURE DIVISION .
       BEGIN.
           
           OPEN INPUT INPUT2-FILE 
           DISPLAY RPT-HEADER 
      *    MAIN
           PERFORM READ-LINE
           PERFORM PROCESS-COL-A UNTIL END-OF-FILE
           MOVE TOTAL TO RPT-TOTAL
           DISPLAY RPT-FOOTER 
      *    END-MAIN

           CLOSE INPUT2-FILE
           GOBACK 
           .
       
       PROCESS-COL-A.
      *    1. init processing item (ตัวที่กำลังดำเนินการ)
           MOVE  COL-A TO COL-A-PROCESSING 
           MOVE  ZEROS TO COL-A-TOTAL 
           MOVE COL-A-PROCESSING TO PRT-COL-A
      *    2. loop จนกระทั้งเจอเคสที่ processing item != ตัวที่อ่านอยู่
      *    ในขั้นตอนนี้จะ sum อะไรก็ทำได้เลย
           PERFORM PROCESS-COL-B UNTIL COL-A NOT = COL-A-PROCESSING
      *    3. display total
           MOVE COL-A-TOTAL TO RPT-A-TOTAL
       
           DISPLAY RPT-A-TOTAL-ROW
           EXIT 
           .

       PROCESS-COL-B.
      *    1. init processing item (ตัวที่กำลังดำเนินการ)
           MOVE  COL-B TO COL-B-PROCESSING 
           MOVE  ZEROS TO COL-B-TOTAL 
           MOVE COL-B-PROCESSING TO PRT-COL-B
      *    2. loop จนกระทั้งเจอเคสที่ processing item != ตัวที่อ่านอยู่
      *    ในขั้นตอนนี้จะ sum อะไรก็ทำได้เลย
           PERFORM PROCESS-LINE UNTIL COL-B NOT = COL-B-PROCESSING OR 
           COL-A NOT = COL-A-PROCESSING
      *    3. display total
           MOVE COL-B-TOTAL TO RPT-COL-TOTAL
           DISPLAY RPT-ROW
           MOVE SPACES TO PRT-COL-A
           EXIT 
           .

       
      

       PROCESS-LINE.
      *    PROCESS
           ADD COL-COUNT TO TOTAL, COL-A-TOTAL , COL-B-TOTAL 
      *    END-PROCESS
           PERFORM READ-LINE 

           EXIT
           .


       READ-LINE.
           
              READ INPUT2-FILE
              AT END SET END-OF-FILE TO TRUE 
              END-READ 
           
           EXIT
           .

